export default class MobileUtils {
  constructor () {
    if (!this._device) {
      this._device = this.device;
    }
  }

  get device () {
    var pfx = "[37degrees:mobile-utils.deviceInfo] ",
        dvc = this._device,
        getViewport = function () {
          // get device's viewport size
          if (document && document.documentElement) {
            dvc.height = document.documentElement.clientHeight || null;
            dvc.width  = document.documentElement.clientWidth || null;
          }
        };

    // we have a valid device (it's not cordova, or it is and we successfully identified the platform
    if (dvc !== undefined && dvc !== null && (!dvc.isCordova
      || (dvc.isCordova && dvc.uuid !== 'browser' && dvc.uuid !== 'unknown'))) {

      console.log(pfx + "device.get() object found", { device: dvc });
      if (!dvc.width || !dvc.height) {
        getViewport();
      }
      return dvc;
    }

    console.log(pfx + "device.get() object NOT found");

    // Device defaults - these will get overridden for real devices
    dvc = {
      isBrowser:        true,
      userAgent:        null,
      isIOS:            false,
      isAndroid:        false,
      platform:         'browser',
      isIOSBrowser:     false,
      isAndroidBrowser: false,
      uuid:             uuid && uuid.v4 ? uuid && uuid.v4() : 'browser',
      height:           null,
      width:            null
    };

    // get device's viewport size
    getViewport();

    // Add device object info
    if (typeof device !== 'undefined' && device !== null) {
      dvc = device;  // global device
      dvc.isBrowser = false;
    }

    // Add user agent string
    if (typeof navigator !== 'undefined' && navigator !== null) {
      dvc.userAgent = navigator.userAgent;

      // Set if this is a mobile browser
      if (dvc.isBrowser) {
        dvc.isAndroidBrowser = navigator.userAgent.match(/Android/) ? true : false;
        dvc.isIOSBrowser     = navigator.userAgent.match(/iP(hone|od|ad)/) ? true : false;
      }
    }

    // Set if this is a mobile device
    let platform = dvc.platform || 'unknown';
    dvc.isIOS     = (platform.toLowerCase() === 'ios') || (Meteor.isCordova && dvc.isIOSBrowser);
    dvc.isAndroid = (platform.toLowerCase() === 'android') || (Meteor.isCordova && dvc.isAndroidBrowser);

    // Ensure we have a UUID - it's required by local login
    if (!dvc.uuid || dvc.uuid.trim() === '') {
      dvc.uuid = 'unknown';
    }

    console.log(pfx + "device.get() object created: " + JSON.stringify(dvc));
    this._device = dvc;
    return dvc;
  }
}
